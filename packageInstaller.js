(
  function () {
    //package1 should install "CamelCaser, KittenService"
    var package1 = [ "KittenService: CamelCaser", "CamelCaser: " ];
    //package2 should install "KittenService, Ice, Cyberportal, Leetmeme, CamelCaser, Fraudstream"
    var package2 = [ "KittenService: ", "Leetmeme: Cyberportal", "Cyberportal: Ice", "CamelCaser: KittenService", "Fraudstream: Leetmeme", "Ice: "]
    //package3 should fail to install. Be sure it does and capture in 'testing'
    var package3 = [ "KittenService: ", "Leetmeme: Cyberportal", "Cyberportal: Ice", "CamelCaser: KittenService", "Fraudstream: ","Ice: Leetmeme" ]
    var packageInstallOrder =[];
    var packageDependants = [];
    var isCycle = false;
    var cyclePackage;

    //break apart each package and pass it to checkDependancy
    //to see if it will loop back on itself
    var sortPackages = function (package) {
      var packageNum = package;
      package.map(function (package) {
        var dependancy = package.split(':')[1].trim() || false;
        if (!dependancy) packageInstallOrder.push(package.split(':')[0]);
        else packageDependants.push(package);
      })
      checkDependants();
      if (isCycle) {
        packageInstallOrder = [];
        console.error("Loop detected with Dependancy list. Aborting Installation");
      }
      
    };

    function checkDependants () {
      // console.log(packageDependants);
      packageDependants.forEach(function (package) {
        var dependant = package.split(':')[0];
        var dependancy = package.split(':')[1].trim() || false;
        cyclePackage = dependant;
        checkDependancies(dependant, dependancy);
      })
    }

    function checkDependancies (dependant, dependancy) {
      var breakCycle = {};
      packageDependants.some(function (indexPackage){
        var indexedDependant = indexPackage.split(':')[0];
        var indexedDependancy = indexPackage.split(':')[1].trim() || false;
        
        if (dependant === indexedDependant && dependancy === indexedDependancy && packageDependants.length > 1) return;
        if (dependancy === indexedDependant) {
          if (indexedDependant === cyclePackage) {
            isCycle = true;
            return;
          }
          checkDependancies(indexedDependant,indexedDependancy);
        }
      })
      addToPackageInstallOrder(dependant);
    }

    var addToPackageInstallOrder = function (indexedDependancy) {
      var indexedAdded = packageInstallOrder.some(function (package) {
        return package === indexedDependancy;
      })
      if (!indexedAdded) {
        packageInstallOrder.push(indexedDependancy);
        // console.log(packageInstallOrder);
      }
    }

    sortPackages(package1);
    // sortPackages(package2);
    // sortPackages(package3);
    document.getElementById("container").innerHTML = packageInstallOrder;
    

  }
)()